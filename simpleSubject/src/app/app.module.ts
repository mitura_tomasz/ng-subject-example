import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { CompanyModule } from './modules/company/company.module';

import { WorkService } from './services/work.service';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    CompanyModule
  ],
  providers: [WorkService],
  bootstrap: [AppComponent],
})
export class AppModule { }
