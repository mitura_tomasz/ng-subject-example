import { Component, OnInit } from '@angular/core';

import { WorkService } from './../../../../services/work.service';


@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css']
})
export class EmployeeComponent implements OnInit {

  workTime: number;

  constructor(private _workService: WorkService) {
    this._workService.$workTimeSubject.subscribe((value) => {
      this.workTime = value;
    });
  }
  
  ngOnInit() {}

}
