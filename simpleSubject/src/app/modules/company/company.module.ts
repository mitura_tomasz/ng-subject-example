import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CompanyComponent } from './company.component';
import { EmployeeComponent } from './component/employee/employee.component';
import { BossComponent } from './component/boss/boss.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [CompanyComponent, EmployeeComponent, BossComponent],
  exports: [CompanyComponent]

})
export class CompanyModule { }
