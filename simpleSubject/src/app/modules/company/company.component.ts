import { Component, OnInit } from '@angular/core';
import { WorkService } from './../../services/work.service';

@Component({
  selector: 'app-company',
  templateUrl: './company.component.html',
  styleUrls: ['./company.component.css']
})
export class CompanyComponent implements OnInit {

  constructor(private _workService: WorkService) { }

  ngOnInit() {
    this._workService.setWorkTime(60);
  }
  
  changeWorkTime(time: number): void {
    this._workService.setWorkTime(time);
  }
}
