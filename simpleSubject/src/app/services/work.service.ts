import { Injectable } from '@angular/core';
import { NgModule } from '@angular/core';
import { Subject } from "rxjs/Subject";
import { Observable } from 'rxjs/Observable';

@Injectable()
export class WorkService {
  
  public $workTimeSubject: Subject<number> = new Subject();
  
  constructor() { 
    this.$workTimeSubject.next(60);
  }
  
  getWorkTime(): Observable<number> {
    return this.$workTimeSubject.asObservable();
  }
  
  setWorkTime(hours: number): void {
    this.$workTimeSubject.next(hours);
  }

}
